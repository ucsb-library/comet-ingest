SHELL = /bin/sh

# A Makefile for performing comet-local tasks.

override empty :=
override space := $(empty) $(empty)
override escape = $(subst $(space),%20,$1)
override octothorpe := \#

info:
	@printf '%b\n' 'Error: You must specify something to make!\n\nOptions include:\n\n• adrl/csv: Make all ADRL csvs (this will take a while)\n    ◦ adrl/csv/$${ID}.csv: Make a specific ADRL csv' >&2
	@exit 1

# ADRL csv generation ##########################################################
#
#     make adrl/csv/pamss001.csv
#
# will output to <./adrl/csv/pamss001.csv>

ADRL_RDF := ../adrl-dm/export/exported-rdf

adrl/csv: FORCE $(patsubst %,adrl/csv/%.csv,$(shell sed 's/	.*//' < adrl/collections.tsv)) ;

override adrl_collections := $(filter-out $(octothorpe)%,$(shell sed 's/	.*//' < adrl/collections.tsv))
override adrl_newspapers := $(filter-out $(octothorpe)%,$(shell sed 's/	.*//' < adrl/newspapers.tsv))

override adrl_row = $(shell sed 's/	.*//' < $(if $(filter $1,$(adrl_collections)),adrl/collections.tsv,adrl/newspapers.tsv) | grep -F -n -x '$1' | sed 's/:.*//')
override adrl_ark = $(if $(filter $1,$(adrl_collections)),$(shell sed '$(call adrl_row,$1)!d;s/^[^	]*	//;s/	.*//' < adrl/collections.tsv),)
override adrl_search = $(shell sed '$(call adrl_row,$1)!d;s/^[^	]*	//;s/	.*//' < adrl/newspapers.tsv)
override adrl_query = $(if $(filter $1,$(adrl_collections)),https://alexandria.ucsb.edu/lib/$(call adrl_ark,$1)?per_page=100,$(call adrl_search,$1)&per_page=100)
override adrl_source = $(if $(filter $1,$(adrl_collections)),http://alexandria.ucsb.edu/lib/$(call adrl_ark,$1),$(call adrl_search,$1))
override adrl_title = $(if $(filter $1,$(adrl_collections)),,$(shell sed '$(call adrl_row,$1)!d;s/^[^	]*	[^	]*	//;s/	.*//' < adrl/newspapers.tsv))
override adrl_ids = $(foreach collection,$(call adrl_query,$1),$(shell NEXT='$(collection)'; printf '%s\n' 'Fetching collection pages for `$1´…' >&2; while [ ! -z "$$NEXT" ]; do DOCUMENT="$$(curl --globoff "$$NEXT" | sed -E 's&<(/?)(header|footer|section|nav)&<\1div&g')"; printf '%s' "$$DOCUMENT" | xmllint --html --xpath '//*[@id="documents"]//*[@itemtype="http://schema.org/Thing"]//h3//a/@href' - 2>/dev/null | tr -d '"' | sed 's| *href=|http://alexandria.ucsb.edu|' || true; NEXTLINK="$$(printf '%s' "$$DOCUMENT" | xmllint --html --xpath 'string(//*[@class="pagination"]//a[@rel="next" and not(ancestor::*[@class="disabled"])]/@href)' - 2>/dev/null || true)"; if [ -z "$$NEXTLINK" ]; then NEXT=; else NEXT="https://alexandria.ucsb.edu$$NEXTLINK"; fi; done))

$(patsubst %,adrl/csv/%.csv,$(adrl_collections) $(adrl_newspapers)): adrl/csv/%.csv: FORCE surfliner
	@if [ ! -d "adrl/csv" ]; then mkdir -p "adrl/csv"; fi
	@printf '%s\n' 'Generating $@…' >&2
	@printf '%s\n' 'require "./adrl/lib/adrl_csv.rb"; File.write("$@", AdrlCsv.generate_from(*"$(call adrl_ids,$*)".split, rdf_directory: "$(wildcard $(ADRL_RDF))", parent: "$(call adrl_source,$*)", default_parent_title: "$(call adrl_title,$*)"))' | ruby - 3>&1 >&2 2>&3 | tee '$@.log'
	@printf '%s\n' 'Done.' >&2

# Surfliner dependencies #######################################################

surfliner: .surfliner/README.md
	cd .surfliner && git pull

.surfliner/README.md:
	git clone https://gitlab.com/surfliner/surfliner.git .surfliner

# Special targets ##############################################################

.PHONY: surfliner FORCE ;
