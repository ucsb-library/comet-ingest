# comet-local

A tracking and tools repository for UCSB‐specific work on
[Project Surfliner][]’s [comet][].

## ADRL Ingest CSV Generation

Run

```sh
make adrl/csv/⟨identifier⟩.csv
```

(where `⟨identifier⟩` is the identifier of a collection in ADRL) to generate the
corresponding CSV. Depends on `xmllint` (installed by default on macOS; see
`libxml2-utils` on Linux).

The code is split between the Makefile, which scrapes ADRL for a list of object
identifiers, and the Ruby script in `adrl/lib/adrl_csv.rb`, which takes that
list of object identifiers and fetches and processes their metadata. Probably it
could all be moved into Ruby at this point, but there’s not much reason to
change it now when the current implementation works.

## License

Copyright © The Regents of the University of California.

Licensed under an [MIT License](./LICENSE).

[Project Surfliner]: <https://surfliner.ucsd.edu>
[comet]: <https://gitlab.com/surfliner/surfliner/-/tree/trunk/comet>
