#!/bin/sh

rdf_dir="${RDF_DIR}"

path="$(echo ${1} | sed 's|http://adrlfedora:8080/||')"

expectedSize="$(rg --pcre2 --no-line-number -o "(?<=hasSize>\ \")[0-9]+" ${rdf_dir}/${path}/fcr:metadata.nt)"
expectedHash="$(rg --pcre2 --no-line-number -o "(?<=hashValue>\ \").{32}" ${rdf_dir}/${path}/fcr:metadata.nt)"

s3="$(aws s3api head-object --bucket comet-staging-batch-source --key $(echo ${path} | sed 's|fedora/rest|fcrepo|') 2>/dev/null)"

if [ -z "${s3}" ]; then
  echo "UH OH no file found in S3 for ${path}"
  exit 1
fi

actualSize="$(echo ${s3} | jq '.ContentLength')"
if [ "${expectedSize}" -eq "${actualSize}" ]; then
  echo "File sizes match for ${path}"
else
  echo "UH OH file size mismatch for ${path}; expected ${expected}, got ${actual}"
  exit 1
fi

md5chksum="$(echo ${s3} | jq '.Metadata.md5chksum' | tr -d '"')"
etag="$(echo ${s3} | jq '.ETag' | tr -d '"\\')"

if [ "${md5chksum}" != "null" ]; then
  md5="$(echo ${md5chksum} | base64 -d | xxd -p)"

  if [ "${expectedHash}" = "${md5}" ]; then
    echo "md5chksum match for ${path}"
    exit 0
  else
    echo "UH OH md5chksum mismatch for ${path}; expected ${expectedHash}, got ${md5}"
    exit 1
  fi
elif [ "${expectedHash}" = "${etag}" ]; then
  echo "etag match for ${path}"
  exit 0
else
  echo "UH OH etag mismatch for ${path}; expected ${expectedHash}, got ${etag}"
  exit 1
fi
