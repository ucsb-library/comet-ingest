# usage:
#
# binaries_from_csv.rb [path to csv] [path to rdf dir]
#
# e.g.:
#
# ruby adrl/lib/binaries_from_csv.rb ~/clones/ucsb/comet-local/adrl/csv/hpamss032.csv /home/cat/clones/ucsb/adrl-dm/export/exported-rdf/fedora/rest/prod

require "csv"
require "pathname"

data = CSV.table(ARGV[0], skip_lines: /^#/, converters: nil)

rdf_dir = Pathname.new(ARGV[1])

data.each do |row|
  next if row[:model] == "Collection"

  stripped_ark = row[:ark].sub(/.*\//, "")

  split_ark = [0, 2, 4, 6].map do |i|
    stripped_ark[i, 2]
  end

  # puts split_ark.inspect

  subpath = Pathname.new(split_ark.join("/"))

  object_dir = rdf_dir.join(subpath)
  nt_path = object_dir.join("#{stripped_ark}.nt")

  # puts nt_path

  next unless object_dir.exist? && nt_path.exist?

  begin
    file_nt_blobs = File.read(nt_path).scan(%r{(?<=hasMember\>\ <http://adrlfedora:8080/fedora/rest/prod/)[^>]+})
  rescue NoMethodError
    warn "No `hasMember` relation in #{nt_path}"
    next
  end

  file_nt_blobs.each do |bleb|
    file_nt = rdf_dir.join(bleb).join('files.nt')

    begin
      file_blob = File.read(file_nt).match(%r{(?<=contains\>\ <)[^>]+})[0]
    rescue NoMethodError
      warn "No `contains` relation in #{file_nt}"
      next
    end

    puts file_blob
  end
end
