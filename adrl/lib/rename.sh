#!/bin/sh

rdf_dir="${RDF_DIR}"

path="$(echo ${1} | sed 's|http://adrlfedora:8080/||')"

filename="$(rg --pcre2 --no-line-number -o "(?<=filename>\ \")[^\"]+" ${rdf_dir}/${path}/fcr:metadata.nt)"

key="$(echo ${path} | sed 's|fedora/rest|fcrepo|')"

s3="$(aws s3api head-object --bucket comet-staging-batch-source --key ${key} 2>/dev/null)"

if [ -z "${s3}" ]; then
  echo "UH OH no file found in S3 for ${path}"
  exit 1
fi

echo "Filename for ${key} should be ${filename}; moving"
aws s3 mv "s3://comet-staging-batch-source/${key}" "s3://comet-staging-batch-source/${key}/${filename}" --validate-same-s3-paths
