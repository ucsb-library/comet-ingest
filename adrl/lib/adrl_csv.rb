require "bundler/inline"
require "csv"
require "net/http"

gemfile do
  source "https://rubygems.org"

  gem "iso-639", ">= 0.3.0", "< 0.4.0"
  gem "linkeddata", "~> 3.2"
  gem "metadata-fields",
    git: "https://github.com/ucsblibrary/metadata-fields.git",
    ref: "e4d41ff8ef89f41bbb6c8dea2d44c1f68d5307d2"
  gem "nokogiri", "~> 1.16", ">= 1.16.5"
  gem "surfliner_schema", path: "./.surfliner/gems/surfliner_schema"
  gem "rdf", "~> 3.2"
end

module AdrlCsv
  # Separates rows.
  ROW_SEPARATOR = "\n"

  # Separates columns.
  COLUMN_SEPARATOR = ","

  # Separates multiple values; must not appear in data.
  FIELD_SEPARATOR = "|"

  # These arks include field separators in their notes with the intended meaning
  # of actually separating the values. Do the intended value separation instead
  # of throwing an error.
  FIELD_SEPARATOR_IN_NOTE = [
    "ark:/48907/f3zw1p1j",
    "ark:/48907/f33j3g4z",
    "ark:/48907/f35h7jcq",
    "ark:/48907/f300047k",
    "ark:/48907/f35h7jcq",
    "ark:/48907/f33r0w1h",
    "ark:/48907/f37h1mrt"
  ]

  # Predicates on EDM dates.
  DATE_PREDICATES = {
    start: "http://www.europeana.eu/schemas/edm/begin",
    finish: "http://www.europeana.eu/schemas/edm/end",
    start_qualifier: "http://www.cidoc-crm.org/cidoc-crm/P79_beginning_is_qualified_by",
    finish_qualifier: "http://www.cidoc-crm.org/cidoc-crm/P80_end_is_qualified_by",
    label: "http://www.w3.org/2004/02/skos/core#prefLabel",
    note: "http://www.w3.org/2004/02/skos/core#note"
  }

  # Predicates on MODS notes.
  NOTE_PREDICATES = {
    value: "http://www.loc.gov/mods/rdf/v1#noteGroupValue",
    note_type: "http://www.loc.gov/mods/rdf/v1#noteGroupType"
  }

  # Predicates to ignore. If a predicate doesn’t have a mapping and isn’t listed
  # here, it will throw an error to ensure we aren’t failing to ingest some kind
  # of important metadata.
  IGNORED_PREDICATES = [
    "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
    "info:fedora/fedora-system:def/model#hasModel",
    "http://www.iana.org/assignments/relation/first",
    "http://www.iana.org/assignments/relation/last",
    "http://projecthydra.org/ns/relations#isGovernedBy",
    "http://www.w3.org/ns/auth/acl#accessControl",
    "http://fedora.info/definitions/v4/repository#created",
    "http://fedora.info/definitions/v4/repository#createdBy",
    "http://fedora.info/definitions/v4/repository#lastModified",
    "http://fedora.info/definitions/v4/repository#lastModifiedBy",
    "http://fedora.info/definitions/v4/repository#writable",
    "http://fedora.info/definitions/v4/repository#hasParent",
    "http://www.w3.org/ns/ldp#contains",
    "http://pcdm.org/models#hasMember",
    "http://opaquenamespace.org/ns/localCollectionID", # local_collection_id
    "http://pcdm.org/models#memberOf", # parent_id
    "http://pcdm.org/models#hasRelatedObject", # index_map_id
    "http://opaquenamespace.org/ns/contributingInstitution", # institution
    "http://www.loc.gov/mods/rdf/v1#recordOrigin", # record_origin
    *DATE_PREDICATES.values, # Handled with the corresponding date property
    *NOTE_PREDICATES.values # Handled with the corresponding note property
  ]

  # Maps ADRL RDF predicates to Comet property names in the UCSB M3.
  #
  # The ADRL property name is provided as a comment.
  PREDICATE_MAPPINGS = {
    "http://purl.org/dc/terms/title" => :title, # title
    "http://purl.org/dc/terms/identifier" => :ark, # identifier
    "http://opaquenamespace.org/ns/cco/accessionNumber" => :identifier_accession, # accession_number
    "http://purl.org/dc/terms/alternative" => :title_alternative, # alternative
    "http://purl.org/dc/terms/creator" => :creator, # creator
    "http://purl.org/dc/terms/contributor" => :contributor, # contributor
    **Fields::MARCREL.map { |(name, term)| [term.to_s, name] }.to_h,
    "http://id.loc.gov/vocabulary/relators/cli" => :client, # client; newer MARC relator not present in the above mapping
    "http://id.loc.gov/vocabulary/relators/isb" => :issuing_body, # issuing_body; newer MARC relator not present in the above mapping
    "http://purl.org/dc/terms/description" => :note_description, # description
    "http://www.w3.org/2003/12/exif/ns#gpsLatitude" => :point_coordinate_latitude, # latitude
    "http://www.w3.org/2003/12/exif/ns#gpsLongitude" => :point_coordinate_longitude, # longitude
    "http://www.rdaregistry.info/Elements/u/#horizontalScaleOfCartographicContent.en" => :scale, # scale
    "http://purl.org/dc/terms/language" => :language, # language
    "http://purl.org/dc/terms/spatial" => :subject_spatial, # location
    "http://id.loc.gov/vocabulary/relators/pup" => :publication_statement, # place_of_publication
    "http://purl.org/dc/terms/subject" => :subject_topic, # subject; requires special handling
    "http://purl.org/dc/terms/publisher" => :publication_statement, # publisher
    "http://purl.org/dc/terms/rightsHolder" => :rights_holder, # rights_holder
    "http://www.loc.gov/premis/rdf/v1#hasCopyrightStatus" => :rights_status, # copyright_status
    "http://purl.org/dc/terms/rights" => :rights_statement, # license
    "http://purl.org/dc/terms/type" => :type_of_resource, # work_type
    "http://opaquenamespace.org/ns/seriesName" => :series, # series_name
    "http://opaquenamespace.org/ns/folderName" => :file_unit, # folder_name
    "http://id.loc.gov/vocabulary/identifiers/issue-number" => :identifier_issue_number, # issue_number
    "http://id.loc.gov/vocabulary/identifiers/matrix-number" => :identifier_matrix_number, # matrix_number
    "http://purl.org/dc/terms/created" => :date_created, # created
    "http://purl.org/dc/terms/date" => :date, # date_other
    "http://www.rdaregistry.info/Elements/w/#formOfWork.en" => :genre_form, # form_of_work
    "http://www.rdaregistry.info/Elements/u/#preferredCitation.en" => :preferred_citation, # citation
    "http://www.loc.gov/mods/rdf/v1#digitalOrigin" => :digital_origin, # digital_origin
    "http://www.loc.gov/mods/rdf/v1#recordDescriptionStandard" => :description_standard, # description_standard
    "http://purl.org/dc/terms/extent" => :physical_description, # extent
    "http://www.loc.gov/mods/rdf/v1#locationCopySublocation" => :sublocation, # sub_location
    "http://www.loc.gov/mods/rdf/v1#accessCondition" => :rights_note, # restrictions
    "http://lod.xdams.org/reload/oad/has_findingAid" => :finding_aid, # finding_aid
    "http://www.loc.gov/mods/rdf/v1#note" => :note_general, # notes
    "http://purl.org/dc/terms/issued" => :date_issued, # issued
    "http://purl.org/dc/terms/dateCopyrighted" => :copyright_date # date_copyrighted
  }

  class SchemaLoader < SurflinerSchema::Loader
    def self.search_paths
      super + [Pathname.new(Dir.pwd) + ".surfliner/comet"]
    end
  end

  def self.schema_properties
    @properties ||= SchemaLoader.new(["ucsb-metadata"]).properties_for(:generic_object)
  end

  def self.notes_and_sors
    return @notes_and_sors unless @notes_and_sors.nil?
    sor_csv = CSV.table("adrl/sor.csv", skip_lines: /^#/, converters: nil)
    data = {}
    sor_csv.each do |row|
      # Iterate over each row and collect the notes and statements of
      # responsibility.
      object_data = {notes: [], sors: []}
      row_data = {note_values: [], note_type_values: []}
      row.each do |(header, col)|
        # Each row contains multiple note columns and multiple note type
        # columns; collect these into two arrays.
        row_data[:note_values] << col if header == :note
        row_data[:note_type_values] << col if header == :note_type
      end
      row_data[:note_values].each_with_index do |val, index|
        # For each note, check its type and store its value appropriately.
        if row_data[:note_type_values][index]&.to_sym == :statement_of_responsibility
          object_data[:sors] << val
        elsif !val.nil?
          object_data[:notes] << val
        end
      end
      data[row[:accession_number]] = object_data
    end
    @notes_and_sors = data
  end

  def self.hardcoded_dates_issued
    return @hardcoded_dates_issued unless @hardcoded_dates_issued.nil?
    multidate_csv = CSV.table("adrl/multidate.csv", skip_lines: /^#/, converters: nil)
    data = {}
    multidate_csv.each do |row|
      # Iterate over each row and collect the date metadata.
      data[row[:accession_number]] = formatted_date(start: row[:issued_start],
        start_qualifier: row[:issued_start_qualifier],
        finish: row[:issued_finish],
        finish_qualifier: row[:issued_finish_qualifier],
        label: row[:issued_label],
        object_identifier: row[:accession_number])
    end
    @hardcoded_dates_issued = data
  end

  def self.generate_from(*uris, rdf_directory:, parent:, default_parent_title: nil)
    headers = [:source_identifier, :identifier_accession, :model, :parents, *PREDICATE_MAPPINGS.values, :statement_of_responsibility].uniq
    table = CSV::Table.new([], headers: headers)
    parent_metadata = {source_identifier: [parent], model: ["Collection"]}
    if parent.include?("ark:")
      parent_metadata[:ark] = [parent.split("ark:")[1].prepend("ark:")]
      puts "Fetching collection metadata…"
      parent_uri = URI(parent.sub("http:", "https:"))
      parent_doc = Nokogiri::HTML4.parse(Net::HTTP.get(parent_uri), parent_uri, "utf-8")
      {
        title: "h1",
        identifier_accession: "dd[@class='blacklight-accession_number_ssim']"
      }.each do |(term, xpath)|
        # Extract metadata terms from the HTML of the collection page.
        #
        # Most terms are not handled, since we only have access to an unstructured
        # string, not the underlying structured values. Title is required for the
        # CSV to be importable, and the accession identifier should help with
        # tracking down the remaining metadata.
        metadata_nodes = parent_doc.xpath("//*[@itemtype='http://schema.org/CollectionPage']//#{xpath}[1]//text()")
        parent_metadata[term] = [metadata_nodes.to_a.map(&:to_s).join("").strip.gsub(/\s+/, " ")] unless metadata_nodes.empty?
      end
      parent_metadata[:source_identifier] = parent_metadata[:identifier_accession] if parent_metadata.include?(:identifier_accession)
    elsif default_parent_title
      parent_metadata[:title] = [default_parent_title.to_s]
    end
    add_to_table(parent_metadata, table: table)
    uris.each_with_index do |uri, index|
      puts "Fetching object (#{index + 1}/#{uris.count})…"
      rdf_subject = "http://adrlfedora:8080/#{path_for(uri)}"
      graph = RDF::Graph.load("#{rdf_directory}/#{rdf_subject.delete_prefix("http://adrlfedora:8080/")}.nt")
      accession_number = graph.query({predicate: RDF::URI("http://opaquenamespace.org/ns/cco/accessionNumber")}).to_a.first.object.to_s
      data = {source_identifier: [accession_number || uri], model: ["GenericObject"], parents: [parent_metadata[:source_identifier]]}
      date_encountered = false
      graph.each_statement do |statement|
        # Iterate over each statement and add it to the data.
        raise ArgumentError, "Unexpected subject in #{uri}: #{statement.subject}" if statement.subject.to_s.split("#").first != rdf_subject
        predicate = statement.predicate.to_s
        next if IGNORED_PREDICATES.include?(predicate)
        mapped = PREDICATE_MAPPINGS[predicate]
        raise ArgumentError, "Unexpected predicate in #{uri}: #{predicate}" if mapped.nil?
        value = case mapped
        when :language
          # Languages must be mapped from the controlled vocabulary into
          # xsd:language tags.
          bcp47(statement.object)
        when :description_standard
          # ADRL uses `aacr2` to identify what our M3 schema calls `aacr`.
          (statement.object == "aacr2") ? "aacr" : statement.object
        when :subject_spatial
          statement.object # TODO: Map from controlled value
        when :subject_topic
          statement.object # TODO: Map from controlled value
        when :publication_statement
          # Two properties map to publication_statement and they need to be
          # joined with space‐colon‐space.
          if data.include?(mapped)
            data[mapped] = if predicate == "http://purl.org/dc/terms/publisher"
              # The current term is publisher; place of publication was already
              # given.
              [statement.object.to_s + " : " + data[mapped].first.to_s]
            else
              # The current term is place of publication; publisher was already
              # given.
              [data[mapped].first.to_s + " : " + statement.object.to_s]
            end
            next # skip further processing
          else
            statement.object
          end
        when :date_created, :date, :date_issued, :copyright_date
          if hardcoded_dates_issued.include?(accession_number)
            # Ignore dates except date_issued when hardcoded, and use the
            # hardcoded value.
            next unless mapped == :date_issued
            hardcoded_dates_issued[accession_number]
          else
            raise ArgumentError, "Multiple dates in #{uri}" if date_encountered
            date_encountered = true
            values = DATE_PREDICATES.each_with_object({}) { |(name, p), vals|
              # Collect the date information, but only if provided.
              result = graph.query({predicate: RDF::URI(p)}).to_a.filter { |statement|
                # Fedora has some garbage data from deleted objects which
                # weren’t fully cleaned up; ignore dates which aren’t actually
                # used in the object of some property.
                graph.query({object: statement.subject}).to_a.count > 0
              }
              raise ArgumentError, "Multiple date properties for date in #{uri}, #{accession_number}" if result.count > 1
              vals[name] = result.first&.object&.to_s
            }
            formatted_date(**values, object_identifier: uri)
          end
        when :note_general
          if data.include?(mapped)
            # All notes are processed at the same time, so ignore any after the
            # first.
            next
          end
          values = NOTE_PREDICATES.each_with_object({}) { |(name, p), vals|
            # Collect the note type and value, but only if provided.
            result = graph.query({predicate: RDF::URI(p)}).to_a.filter { |statement|
              # Fedora has some garbage data from deleted objects which
              # weren’t fully cleaned up; ignore dates which aren’t actually
              # used in the object of some property.
              graph.query({object: statement.subject}).to_a.count > 0
            }
            vals[name] = result&.map(&:object)
          }
          if values[:note_type].include?("statement of responsibility") || values[:note_type].include?("statement_of_responsibility")
            # The notes include a statement of responsibility. In this case, we
            # cannot correctly source the notes from ADRL and instead must
            # parse them from the original ingest data.
            #
            # Note: This is probably not true anymore now that we are working
            # from the actual fedora source data (which uses fragment
            # identifiers to distinguish notes); we probably CAN “correctly”
            # handle these, it would just be more work.
            raise ArgumentError, "#{uri} contains a statement of responsibility, but the original ingest sources do not provide one for its accession number(#{accession_number})" unless notes_and_sors.include?(accession_number)
            data[mapped] = notes_and_sors[accession_number][:notes]
            data[:statement_of_responsibility] = notes_and_sors[accession_number][:sors]
          else
            # The notes do not include a statement of responsibility.
            raise ArgumentError, "Unexpected note types in #{uri}: #{values[:note_type].map(&:to_s)}" if values[:note_type].any? { |nt| !["acquisition", "performer", "venue"].include?(nt) }
            # There may be multiple note values.
            if data.include?(mapped)
              data[mapped] += values[:value]
            else
              data[mapped] = values[:value]
            end
          end
          next # skip further processing
        else
          statement.object
        end
        if data.include?(mapped)
          data[mapped] << value
        else
          data[mapped] = [value]
        end
      end
      add_to_table(data, table: table)
    end
    table.by_col!
    table.delete_if { |column_data| column_data[1].all? { |v| v.to_s.empty? } } # remove unused cols
    table.by_row!
    return table.to_csv(row_sep: ROW_SEPARATOR, col_sep: COLUMN_SEPARATOR) if table.size <= 2 # header + one row = 2
    sorted_table = CSV::Table.new([], headers: table.headers)
    table
      .sort_by { |row| row[:identifier_accession] || row[:ark] || "" }
      .each { |row| sorted_table << row }
    sorted_table.to_csv(row_sep: ROW_SEPARATOR, col_sep: COLUMN_SEPARATOR)
  end

  def self.bcp47(iri)
    raise ArgumentError "Unexpected language URI: #{iri}" unless iri.to_s.start_with?("http://id.loc.gov/vocabulary/iso639-2/")
    alpha3 = iri.to_s.delete_prefix("http://id.loc.gov/vocabulary/iso639-2/")
    iso = ISO_639.find_by_code(alpha3)
    return alpha3 unless iso
    [iso.alpha2, iso.alpha3_terminologic, iso.alpha3_bibliographic].find { |code| !code.empty? }
  end

  def self.formatted_date(start:, finish: nil, start_qualifier: nil, finish_qualifier: nil, label: nil, note: nil, object_identifier:)
    if finish.nil? || start == finish
      # Only a start is provided (or the finish is the same); it may be
      # qualified. Approximate and inferred dates both map to EDTF
      # “approximate”, and questionable and uncertain dates both map to EDTF
      # “uncertain”.
      case start_qualifier.to_s.to_sym
      when :approximate, :inferred
        "#{start}~"
      when :questionable, :uncertain
        "#{start}?"
      else
        raise ArgumentError, "Unexpected start qualification #{start_qualifier} for date in #{object_identifier}" unless start_qualifier.nil?
        start
      end
    elsif !start.nil?
      # If both a start and finish are provided, they indicate a *range of
      # possible dates* and are encoded with EDTF “one of a set” syntax. For
      # metadata model simplicity, we’ve decided to rely on date labels here and
      # not try to encode qualifications on these dates.
      raise ArgumentError, "Undefined start for date in #{object_identifier}" if start.nil?
      if start.size != finish.size
        $stderr.puts "Date start and finish have different precision in #{object_identifier}"
        if start.size < finish.size
          start += "-01" # add month or day
          start += "-01" if start.size < finish.size # add day
        else # start.size > finish.size
          finish += "-12" if finish.size == 4 # add month
          finish += days_in_month(finish[5, 2].to_i, year: finish[0, 4].to_i) if finish.size < start.size # add day
        end
        raise ArgumentError, "Unable to fixup precision on start and end for date in #{object_identifier}" unless start.size == finish.size
      end
      "[#{start}..#{finish}]"
    else
      # If no start is provided, it indicates a range of dates with no start.
      "[..#{finish}]"
    end
  end

  def self.days_in_month(month, year:)
    if month == 2
      year % 4 != 0 || year % 100 == 0 && year % 400 != 0 ? 28 : 29
    elsif month == 4 || month == 6 || month == 9 || month == 11
      30
    else
      31
    end
  end

  def self.controlled(name, value)
    schema_property = schema_properties[name]
    return value unless schema_property&.controlled_values
    schema_property.controlled_values.values.values.each do |controlled_value|
      return controlled_value.iri if [controlled_value.name.to_s,
        controlled_value.display_label, controlled_value.iri.to_s].include?(value)
    end
    value
  end

  def self.path_for(uri)
    id = uri.split("/").last
    "fedora/rest/prod/#{id[0,2]}/#{id[2,2]}/#{id[4,2]}/#{id[6,2]}/#{id}"
  end

  def self.add_to_table(data, table:)
    headers = table.headers
    table << CSV::Row.new(headers, headers.map { |field|
      # Map each header to its value in data.
      value = data[field]
      next if value.nil?
      value = value.flat_map { |v| v.to_s.split("|").map(&:strip) } if field == :note_general && FIELD_SEPARATOR_IN_NOTE.include?(data[:ark][0].to_s)
      value = value.map { |v| controlled(field, v) }
      raise EncodingError, "`#{FIELD_SEPARATOR}` must not appear in values, but it did in values #{value.map(&:to_s).inspect} for field `#{field}` in <#{data[:ark][0]}>." if value.any? { |v| v.to_s.include?(FIELD_SEPARATOR) }
      value.sort.join(FIELD_SEPARATOR)
    })
  end
end
