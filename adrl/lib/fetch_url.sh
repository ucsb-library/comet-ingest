#!/bin/env bash

# Usage:
#
# fetch_url.sh <url>

key="fcrepo$(echo ${1} | sed 's|.*rest||')"

s3="$(aws s3api head-object --bucket comet-staging-batch-source --key ${key} 2>/dev/null)"

if [ ! -z "${s3}" ]; then
  echo "Wahoo ${key} is already uploaded"
  exit 0
fi

# will look like
# http://adrlfedora:8080/fedora/rest/prod/8s/45/q8/76/8s45q876k/files/d958c43d-843a-48d6-9b23-90fc99c3b437
url="$(echo ${1} | sed 's|adrlfedora|10.3.61.19|g')"

dirs="$(echo ${url} | sed 's|.*prod/\(.*\)/files.*|\1|' -)"

mkdir -p "${dirs}/files"

pushd "${dirs}/files"

curl --fail -C - -OL "${url}"

popd
